package com.flycms.modules.notify.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Setter
@Getter
public class SmsTemplate implements Serializable {
    private static final long serialVersionUID = 1L;
    private String  id;
    private String  apiId;
    private String  templateName;
    //模板编码
    private String  templateCode;
    private String  detail;
    private LocalDateTime createTime;
    private String  status;

}
