package com.flycms.modules.links.service;

import com.flycms.common.pager.Pager;
import com.flycms.modules.links.entity.Links;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 22:52 2019/12/5
 */
public interface LinksService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 添加友情链接
     *
     * @param links
     * @return
     */
    public Object addLink(Links links);

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除友情链接
     *
     * @param id
     * @return
     */
    public int deleteById(Long id);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 更新网站友情链接
     *
     * @param links
     * @return
     */
    public Object updateLink(Links links);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 网站友情链接翻页列表
     *
     * @param links
     *         根据分类名查询
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectLinksLayListPager(Links links, Integer page, Integer pageSize, String sort, String order);

    /**
     * 查询网站所属友情链接信息
     *
     * @param siteId
     *         所输网站id
     * @param linkType
     *         链接类型
     * @param page
     *         当前页数
     * @param pageSize
     *         每页显示数量
     * @param sort
     *         指定排序字段
     * @param order
     *         排序方式
     * @return
     */
    public Pager<Links> queryLinksPager(Long siteId, int linkType, Integer page, Integer pageSize, String sort, String order);
}
