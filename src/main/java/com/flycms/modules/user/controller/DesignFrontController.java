package com.flycms.modules.user.controller;

import com.flycms.common.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 16:16 2019/11/25
 */
@Controller
@RequestMapping("/design")
public class DesignFrontController extends BaseController {

    //网站首页
    @GetMapping("/index{url.suffix}")
    public String indexSite(Model model)
    {
        return "system/design/index";
    }

    /**
     * 独立网站欢迎页面
     */
    @RequestMapping("/welcome${url.suffix}")
    public String welcome(Model model){

        return "system/design/welcome";
    }
}
