package com.flycms.modules.user.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.common.utils.mark.SnowFlakeUtils;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.Result;
import com.flycms.common.utils.text.Convert;
import com.flycms.modules.user.entity.UserRole;
import com.flycms.modules.user.entity.UserRoleMerge;
import com.flycms.modules.user.dao.UserRoleDao;
import com.flycms.modules.user.dao.UserRoleMenuMergeDao;
import com.flycms.modules.user.dao.UserRoleMergeDao;
import com.flycms.modules.user.entity.UserRoleVO;
import com.flycms.modules.user.service.UserRoleService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 角色 业务层处理
 * 
 * @author 孙开飞
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private UserRoleMenuMergeDao userRoleMenuMergeDao;

    @Autowired
    private UserRoleMergeDao userRoleMergeDao;

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /*
     * 新增保存角色信息
     *
     * @param userRole 角色信息
     * @return 结果
     */
    @Transactional
    public Object addUserRole(UserRole userRole){
        if(this.checkRoleNameUnique(userRole.getRoleName(),null)){
            return Result.failure("该角色名称已存在");
        }
        if(this.checkRoleKeyUnique(userRole.getRoleKey(),null)){
            return Result.failure("该角色key已存在");
        }
        userRole.setId(SnowFlakeUtils.nextId());
        userRole.setCreateTime(LocalDateTime.now());
        // 新增角色信息
        userRoleDao.save(userRole);
        return Result.success();
    }

    public int addUserRoleMerge(Long userId, Long roleId){
        //先尝试先删除该用户原来绑定的所有权限，理论上新用户是不存在绑定关系
        userRoleMergeDao.deleteUserRoleByUserId(userId);
        UserRoleMerge ur = new UserRoleMerge();
        ur.setId(SnowFlakeUtils.nextId());
        ur.setUserId(userId);
        ur.setRoleId(roleId);
        return userRoleMergeDao.save(ur);
    }

    /**
     * 批量选择授权用户角色
     *
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    public int insertAuthUsers(Long roleId, String userIds)
    {
        Long[] users = Convert.toLongArray(userIds);
        // 新增用户与角色管理
        List<UserRoleMerge> list = new ArrayList<UserRoleMerge>();
        for (Long userId : users)
        {
            UserRoleMerge ur = new UserRoleMerge();
            ur.setId(SnowFlakeUtils.nextId());
            ur.setUserId(userId);
            ur.setRoleId(roleId);
            list.add(ur);
        }
        return userRoleMergeDao.batchUserRole(list);
    }

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    /**
     * 通过角色ID删除角色
     *
     * @param id 角色ID
     * @return 结果
     */
    @Override
    public boolean deleteRoleById(Long id)
    {
        return userRoleDao.deleteRoleById(id) > 0 ? true : false;
    }

    /**
     * 批量删除角色信息
     *
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    @Override
    @Transactional
    public Result deleteRoleByIds(String ids){
        Long[] roleIds = Convert.toLongArray(ids);
        for (Long roleId : roleIds)
        {
            UserRole role = findById(roleId);
            if (countUserRoleByRoleId(roleId) > 0)
            {
                Result.failure(String.format(role.getRoleName()+"%1$s已分配,不能删除"));
            }
        }
        int totalCount=userRoleDao.deleteRoleByIds(roleIds);
        if(totalCount == 0){
            Result.failure("删除失败");
        }
        return Result.success();
    }

    /**
     * 取消授权用户角色
     *
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    @Override
    public int deleteAuthUser(UserRoleMerge userRole)
    {
        return userRoleMergeDao.deleteUserRoleInfo(userRole);
    }

    /**
     * 批量取消授权用户角色
     *
     * @param id 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    public int deleteAuthUsers(Long id, String userIds)
    {
        return userRoleMergeDao.deleteUserRoleInfos(id, Convert.toLongArray(userIds));
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 修改保存角色信息
     *
     * @param userRole 角色信息
     * @return 结果
     */
    public Object updateUserRole(UserRole userRole)
    {
        if(StringUtils.isEmpty(userRole.getId())){
            return Result.failure("角色信息id不能为空");
        }
        if(this.checkRoleNameUnique(userRole.getRoleName(),userRole.getId())){
            return Result.failure("该角色名称已存在");
        }
        if(this.checkRoleKeyUnique(userRole.getRoleName(),userRole.getId())){
            return Result.failure("该角色key已存在");
        }
        // 修改角色信息
        userRoleDao.updateUserRole(userRole);
        return Result.success();
    }

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 校验角色名称是否唯一
     *
     * @param roleName
     *         校验角色名称
     * @param id 需要排除的角色id
     * @return
     */
    public boolean checkRoleNameUnique(String roleName,Long id) {
        UserRole role = userRoleDao.checkRoleNameUnique(roleName,id);
        return role != null ? true : false;
    }

    /**
     * 校验角色权限是否唯一
     *
     * @param roleKey 角色信息
     * @param id 需要排除的角色id
     * @return 结果
     */
    public boolean checkRoleKeyUnique(String roleKey,Long id)
    {
        UserRole role = userRoleDao.checkRoleKeyUnique(roleKey,id);
        return role != null ? true : false;
    }


    /**
     * 通过角色ID查询角色
     *
     * @param id 角色ID
     * @return 角色对象信息
     */
    @Override
    public UserRole findById(Long id) {
        if(!StringUtils.isEmpty(id)){
            return userRoleDao.findById(id);
        }else{
            return null;
        }
    }

    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     *
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    public Object selectRoleListPager(UserRole role, Integer page, Integer limit, String sort, String order) {
        //javabean 映射工具
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        MapperFacade mapper = mapperFactory.getMapperFacade();

        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!org.springframework.util.StringUtils.isEmpty(role.getRoleName())) {
            whereStr.append(" and role_name like concat('%',#{entity.roleName},'%')");
        }
        if (!org.springframework.util.StringUtils.isEmpty(role.getStatus())) {
            whereStr.append(" and status = #{entity.status}");
        }
        if (!org.springframework.util.StringUtils.isEmpty(role.getRoleKey())) {
            whereStr.append(" and role_key like concat('%',#{entity.roleKey},'%')");
        }
        whereStr.append(" and deleted = 0");

        Pager<UserRole> pager = new Pager(page, limit);
        //排序设置
        if (!org.springframework.util.StringUtils.isEmpty(sort)) {
            Boolean rank = "asc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        UserRole entity = new UserRole();
        entity.setRoleName(role.getRoleName());
        entity.setStatus(role.getStatus());
        entity.setRoleKey(role.getRoleKey());
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        return LayResult.success(0, "true", userRoleDao.queryTotal(pager), mapper.mapAsList(userRoleDao.queryList(pager),UserRoleVO.class));
    }

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色id列表
     */
    @Override
    public String selectUserRolesByUserId(Long userId){
        return userRoleDao.selectUserRolesByUserId(userId);
    }

    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectRoleKeys(Long userId)
    {
        List<UserRole> perms = userRoleDao.selectRolesByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (UserRole perm : perms)
        {
            if (com.flycms.common.utils.StringUtils.isNotNull(perm))
            {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

/*    *//**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 角色列表
     *//*
    @Override
    public List<Role> selectRolesByUserId(Long userId)
    {
        List<Role> userRoles = roleDao.selectRolesByUserId(userId);
        List<Role> roles = selectRoleAll();
        for (Role role : roles)
        {
            for (Role userRole : userRoles)
            {
                if (role.getRoleId().longValue() == userRole.getRoleId().longValue())
                {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return roles;
    }*/


     /**
     * 查询所有角色
     * 
     * @return 角色列表
     */

    @Override
    public List<UserRole> selectRoleAll() {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        whereStr.append(" and deleted = 0");
        Pager<UserRole> pager = new Pager();
        //排序设置
        pager.addOrderProperty(null, false,false);
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setWhereStr(whereStr.toString());
        return userRoleDao.queryList(pager);
    }


/*    *//**
     * 新增角色菜单信息
     * 
     * @param role 角色对象
     *//*
    public int insertRoleMenu(Role role)
    {
        int rows = 1;
        // 新增用户与角色管理
        List<RoleMenu> list = new ArrayList<RoleMenu>();
        for (Long menuId : role.getMenuIds())
        {
            RoleMenu rm = new RoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (list.size() > 0)
        {
            rows = roleMenuDao.batchRoleMenu(list);
        }
        return rows;
    }*/

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param id 角色ID
     * @return 结果
     */
    @Override
    public int countUserRoleByRoleId(Long id)
    {
        return userRoleMergeDao.countUserRoleByRoleId(id);
    }

}
