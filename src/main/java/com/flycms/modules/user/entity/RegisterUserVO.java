package com.flycms.modules.user.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 用户注册业务层实体类
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 12:08 2019/9/1
 */
@Setter
@Getter
public class RegisterUserVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String userName;	    //用户名
    private String vercode;	    //手机验证码
    private String password;	    //密码
    private String repass;	    //确认密码
    private String agreement;	    //注册条款
}
