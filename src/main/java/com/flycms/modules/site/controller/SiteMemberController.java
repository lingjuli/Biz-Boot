package com.flycms.modules.site.controller;


import com.flycms.common.controller.BaseController;
import com.flycms.common.exception.PageAssert;
import com.flycms.common.utils.result.Result;
import com.flycms.common.validator.Sort;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.site.entity.Site;
import com.flycms.modules.site.service.SiteService;
import com.flycms.modules.site.service.SiteCompanyService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 用户后台管理页面
 *
 * @author 孙开飞
 */
@Controller
@RequestMapping("/member/site")
public class SiteMemberController extends BaseController {

    @Autowired
    private SiteService siteService;
    @Autowired
    private SiteCompanyService siteCompanyService;

    //网站首页
    @GetMapping("/index{url.suffix}")
    public String indexSite(@RequestParam(value = "id", required = false) String id,Model model)
    {
        if (!NumberUtils.isNumber(id)) {
            return "redirect:/404.do";
        }
        PageAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(id)), "您不是网站管理员，请勿非法操作");
        Company company=companyService.findCompanyByUser(ShiroUtils.getLoginUser().getId());
        Site site=siteService.findSiteByCompanyId(Long.parseLong(id),company.getId());
        if(site == null){
            return "redirect:/404.do";
        }
        model.addAttribute("site",site);
        return "system/member/site/index";
    }

    /**
     * 独立网站欢迎页面
     */
    @RequestMapping("/welcome${url.suffix}")
    public String welcome(@RequestParam(value = "id", required = false) String id,Model model){
        if (!NumberUtils.isNumber(id)) {
            return "redirect:/404.do";
        }
        PageAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(id)), "您不是网站管理员，请勿非法操作");
        return "system/member/site/welcome";
    }

    @GetMapping("/add{url.suffix}")
    public String add(Model model)
    {
        return "system/member/site/popup/add_site";
    }

    @PostMapping("/add{url.suffix}")
    @ResponseBody
    public Object add(Site site)
    {
        if(StringUtils.isEmpty(site.getSiteName())){
            return Result.failure("网站名称不能为空");
        }
        if(StringUtils.isEmpty(site.getDomain())){
            return Result.failure("网站二级域名不能为空");
        }
        return siteService.addUserSite(site);
    }

    //批量删除网站
    @PostMapping("/del{url.suffix}")
    @ResponseBody
    public Object deleteSiteByIds(String ids){
        if(org.apache.commons.lang3.StringUtils.isEmpty(ids)){
            return Result.failure("网站id为空或者不存在");
        }
        return siteService.deleteSiteByIds(ids);
    }

    @PostMapping("/updateStatus{url.suffix}")
    @ResponseBody
    public Object updateSiteByStatus(@RequestParam(value = "status", required = false) Boolean status,
                                     @RequestParam(value = "id", required = false) String id)
    {
        PageAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(id)), "您不是网站管理员，请勿非法操作");
        return siteService.updateSiteByStatus(status,Long.parseLong(id));
    }

    //查询网站二级域名是否被占用
    @PostMapping("/queryDomain{url.suffix}")
    @ResponseBody
    public Object queryDomain(@RequestParam(value = "domain", required = false) String domain,
                              @RequestParam(value = "siteId", required = false) String siteId)
    {
        Long id = null;
        if(!StringUtils.isEmpty(siteId)){
            if (NumberUtils.isNumber(siteId)) {
                id = Long.parseLong(siteId);
                PageAssert.notTrue(!siteCompanyService.checkSiteCompany(id), "您不是网站管理员，请勿非法操作");
            }
        }

        if(siteService.holdSiteByDomain(domain)){
            return Result.failure("请修改当前网站二级域名，该域名已被平台保留");
        }
        if(!siteService.checkSiteByDomain(domain,id)){
            return Result.success("该二级域名可以使用");
        }
        return Result.failure("该二级域名已被占用");
    }

    //编辑网站基本信息
    @GetMapping("/edit{url.suffix}")
    public String edit(@RequestParam(value = "id", required = false) String id, Model model)
    {
        if (!NumberUtils.isNumber(id)) {
            return "redirect:/404.do";
        }
        PageAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(id)), "您不是网站管理员，请勿非法操作");
        Company company=companyService.findCompanyByUser(ShiroUtils.getLoginUser().getId());
        Site site=siteService.findSiteByCompanyId(Long.parseLong(id),company.getId());
        model.addAttribute("site",site);
        return "system/member/site/popup/edit_site";
    }

    //保存网站信息
    @PostMapping("/edit{url.suffix}")
    @ResponseBody
    public Object edit(Site site)
    {
        return siteService.updateSiteById(site);
    }

    @GetMapping("/list{url.suffix}")
    public String siteList()
    {
        return "system/member/site/list";
    }

    @GetMapping("/listData{url.suffix}")
    @ResponseBody
    public Object siteList(Site site,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                           @Sort(accepts = {"create_time", "id"}) @RequestParam(defaultValue = "create_time") String sort,
                           @RequestParam(defaultValue = "desc") String order) {
        return siteService.selectCompanySiteListPager(site, page, pageSize, sort, order);
    }
}